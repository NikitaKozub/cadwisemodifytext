﻿using Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace ViewModel
{
    public class ModifyTextViewModel
    {
        public SettingsModel SettingsModel { get; set; }

        public ModifyTextViewModel()
        {
            SettingsModel = new SettingsModel()
            {
                IsDeletePunctuationMark = true,
                CountNumber = "3",
                IsProgressModification = false,
                IsModification = true
            };

            SelectPathFolderResult = new RelayCommand(arg => СhoicePathFolderResult());
            SelectPathSourceFolder = new RelayCommand(arg => СhoicePathSourceFolder());
        }

        public ICommand SelectPathFolderResult { get; set; }
        public void СhoicePathFolderResult()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            DialogResult result = folderBrowser.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
            {
                SettingsModel.PathResultFolder = folderBrowser.SelectedPath;
            }
        }

        public ICommand SelectPathSourceFolder { get; set; }
        public void СhoicePathSourceFolder()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            DialogResult result = folderBrowser.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
            {
                SettingsModel.PathSourceFolder = folderBrowser.SelectedPath;
            }
        }

        RelayCommand _ModificationText;
        public RelayCommand ModificationText
        {
            get
            {
                return _ModificationText ??
                    (_ModificationText = new RelayCommand((selectedItem) =>
                    {
                        if (_ModificationText != null)
                        {
                            BackgroundWorker worker = new BackgroundWorker();
                            worker.WorkerReportsProgress = true;
                            worker.DoWork += WorkerModification;
                            worker.ProgressChanged += WorkerProgressModification;
                            worker.RunWorkerCompleted += WorkerRunWorkerCompleted;
                            worker.RunWorkerAsync(10000);
                        }
                    }));
            }
        }

        void WorkerModification(object sender, DoWorkEventArgs e)
        {
            int progress = 0;
            (sender as BackgroundWorker).ReportProgress(progress, 0);

            FileTxt fileText = new FileTxt();
            int count = int.Parse(SettingsModel.CountNumber);
            string[] paths = CreateFileGroup(SettingsModel.PathSourceFolder);
            fileText.ModificationFile(paths, SettingsModel.PathResultFolder, SettingsModel.IsDeletePunctuationMark, count);
            progress = 1;
            (sender as BackgroundWorker).ReportProgress(progress);
            e.Result = false;
        }

        /// <summary>
        /// Обновления
        /// </summary>
        void WorkerProgressModification(object sender, ProgressChangedEventArgs e)
        {
            SettingsModel.IsProgressModification = true;
            SettingsModel.IsModification = false;
        }

        /// <summary>
        /// Закончилось
        /// </summary>
        void WorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SettingsModel.IsProgressModification = (bool)e.Result;
            SettingsModel.IsModification = true;
        }


        /// <summary>
        /// Для обработки путей группы файлов(пакетов файлов)
        /// </summary>
        /// <param name="paths">путь к файлам</param>
        /// <returns>массив с этими путями</returns>
        private string[] CreateFileGroup(string path)
        {
            List<string> arrayPathSourceFile = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(path);

            foreach (FileInfo files in dir.GetFiles())
            {
                arrayPathSourceFile.Add(path + "\\" + files.Name);
            }
            return arrayPathSourceFile.ToArray();
        }
    }
}
