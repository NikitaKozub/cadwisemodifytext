﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class SettingsModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public SettingsModel()
        {
            PathSourceFolder = Properties.Settings.Default.PathSourceFolder;
            PathResultFolder = Properties.Settings.Default.PathResultFolder;
        }

        /// <summary>
        /// Путь к исходному файлу
        /// </summary>
        private string pathSourceFolder;
        public string PathSourceFolder
        {
            get { return pathSourceFolder; }
            set
            {
                if (pathSourceFolder != value)
                {
                    pathSourceFolder = value;
                    Properties.Settings.Default.PathSourceFolder = pathSourceFolder;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathSourceFolder");
                }

            }
        }

        /// <summary>
        /// Путь к папке где создать или перезаписать результирующий файл
        /// </summary>
        private string pathResultFolder;
        public string PathResultFolder
        {
            get { return pathResultFolder; }
            set
            {
                if (pathResultFolder != value)
                {
                    pathResultFolder = value;
                    Properties.Settings.Default.PathResultFolder = pathResultFolder;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathResultFolder");
                }
            }
        }

        /// <summary>
        /// Нужно ли удалить знаки препинания
        /// </summary>
        private bool isDeletePunctuationMark;
        public bool IsDeletePunctuationMark
        {
            get { return isDeletePunctuationMark; }
            set
            {
                isDeletePunctuationMark = value;
                OnPropertyChanged("IsDeletePunctuationMark");
            }
        }

        /// <summary>
        /// Минимальное количество символов в слове
        /// </summary>
        private string countNumber;
        public string CountNumber
        {
            get { return countNumber; }
            set
            {
                int count;
                if (!Int32.TryParse(value, out count))
                {
                    countNumber = "0";
                }
                else
                {
                    countNumber = value;
                }
                OnPropertyChanged("CountNumber");
            }
        }

        /// <summary>
        /// Можно ли изменять текст
        /// </summary>
        private bool isModification;
        public bool IsModification
        {
            get
            {
                return isModification;
            }
            set
            {
                if (isModification != value)
                {
                    isModification = value;
                }
                OnPropertyChanged("IsModification");
            }
        }

        /// <summary>
        /// Включен ли прогресс бар
        /// </summary>
        private bool isProgressModification;
        public bool IsProgressModification
        {
            get
            {
                return isProgressModification;
            }
            set
            {
                if (isProgressModification != value)
                {
                    isProgressModification = value;
                }
                OnPropertyChanged("IsProgressModification");
            }
        }
    }
}
