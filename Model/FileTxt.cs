﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// .txt файл
    /// </summary>
    public class FileTxt : _File
    {
        public int CountNumber;
        public bool IsDeletePunctuationMark;
        public StringBuilder word;
        bool EndFile;
        bool IsWordSpelled;
        public StringBuilder NewText;

        private char[] PunctuationMarks = new char[12];
        public FileTxt()
        {
            IsWordSpelled = false;
            word = new StringBuilder();
            NewText = new StringBuilder();
            EndFile = false;
            // Инициализация знаков
            PunctuationMarks[0] = '—';
            PunctuationMarks[1] = '.';
            PunctuationMarks[2] = '?';
            PunctuationMarks[3] = '!';
            PunctuationMarks[4] = ')';
            PunctuationMarks[5] = '(';
            PunctuationMarks[6] = ',';
            PunctuationMarks[7] = ':';
            PunctuationMarks[8] = ' ';
            PunctuationMarks[9] = '–';
            PunctuationMarks[10] = '"';
            PunctuationMarks[11] = '+';
        }

        /// <summary>
        /// Считывание по строкам, нас лучай, если файл большой
        /// </summary>
        /// <param name="path">путь к файлу</param>
        public void ReadFile(string path)
        {
            NewText.Clear();
            foreach (var line in File.ReadLines(path))
            {
                NewText.Append(line);
            }
        }

        
        /// <summary>
        /// Изменить файл
        /// </summary>
        /// <param name="pathSourceFile">путь к исходному файлу</param>
        /// <param name="resultFolder"> путь к паке где будет лежуть файл</param>
        /// <param name="isDeletePunctuationMark">Нужно ли удлить знаки(true - yes, false - no)</param>
        /// <param name="countNumber">Количество символов, для удаления</param>
        public void ModificationFile(string[] pathSourceFile, string resultFolder, bool isDeletePunctuationMark, int countNumber)
        {
            StringBuilder line = new StringBuilder(1);
            IsDeletePunctuationMark = isDeletePunctuationMark;
            CountNumber = countNumber;
            string nameResultFile;
            for (int i = 0; i < pathSourceFile.Length; i++)
            {
                nameResultFile = resultFolder + "\\ResultFile" + (i + 1) + ".txt";
                CheckFile(nameResultFile);
                long length = new FileInfo(pathSourceFile[i]).Length;
                using (var stream = File.Open(pathSourceFile[i], FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var mmf = MemoryMappedFile.CreateFromFile(stream, null, length, MemoryMappedFileAccess.Read, null, HandleInheritability.Inheritable, false))
                    {
                        using (var viewStream = mmf.CreateViewStream(0, length, MemoryMappedFileAccess.Read))
                        {
                            using (BinaryReader binReader = new BinaryReader(viewStream))
                            {
                                using (var writer = new StreamWriter(File.OpenWrite(nameResultFile)))
                                {
                                    while (binReader.PeekChar() > -1)
                                    {
                                        line.Append(binReader.ReadChars(10));
                                        if (IsDeletePunctuationMark)
                                        {
                                            DeletePunctuationMark(line);
                                            WriteFile(line, writer);
                                        }
                                        else
                                        {
                                            WriteFile(line, writer);
                                        }
                                        line.Clear();
                                    }
                                    EndFile = true;
                                    WriteFile(line, writer);
                                }                               
                            }
                        }
                    }
                }
            }
        }
       
        /// <summary>
        /// Записать файл
        /// </summary>
        /// <param name="line">строка которую будут записывать</param>
        /// <param name="writer">StreamWriter</param>
        public void WriteFile(StringBuilder line, StreamWriter writer)
        {

            if (EndFile && word.Length > 0)
            {
                WriteWord('z', writer);
            }
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i].Equals(Environment.NewLine))
                {
                    writer.WriteLine();
                    IsWordSpelled = false;
                    continue;
                }
                if (CheckMark(line[i]) | line[i] == ' ')// Пробел
                {
                    WriteWord(line[i], writer);
                    IsWordSpelled = false;
                    continue;
                }
                if (i == line.Length-1)
                {
                    word.Append(line[i]);
                    if (CheckCountNumber() || CheckNumber())
                    {
                        writer.Write(word);
                        IsWordSpelled = true;
                        word.Clear();
                    }
                    continue;
                }
                word.Append(line[i]);
            }
        }

        /// <summary>
        /// Записать слово
        /// </summary>
        /// <param name="separatingCharacter"></param>
        /// <param name="writer"></param>
        private void WriteWord(char separatingCharacter, StreamWriter writer)
        {
            if (IsWordSpelled)
            {
                writer.Write(word);
                if (!EndFile)
                {
                    writer.Write(separatingCharacter);
                }
                word.Clear();
                return;
            }
            if (CheckNumber())
            {
                writer.Write(word);
                if (!EndFile)
                {
                    writer.Write(separatingCharacter);
                }
                word.Clear();
                return;
            }
            if (CheckCountNumber())
            {
                writer.Write(word);
                if (!EndFile)
                {
                    writer.Write(separatingCharacter);
                }
                word.Clear();
                return;
            }
            else
            {
                if (!EndFile)
                {
                    writer.Write(separatingCharacter);
                }
                word.Clear();
            }
            if (EndFile)
            {
                EndFile = false;
            }
        }


        /// <summary>
        /// Проверка на количество символов в слове
        /// </summary>
        /// <returns>true - да подходит, false - нет не подходит</returns>
        public bool CheckCountNumber()
        {
            if (word.Length >= CountNumber)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Проверка на число
        /// </summary>
        /// <returns></returns>
        public bool CheckNumber()
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (Char.IsDigit(word[i]) | (word[i]=='-' && (i + 1) <= (word.Length - 1) && Char.IsDigit(word[i + 1])))
                {
                    if ((i + 1) == word.Length)
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Удаление знаков
        /// </summary>
        /// <param name="line">строка StringBuilder</param>
        public void DeletePunctuationMark(StringBuilder line)
        {
            for (int i = 0; i < line.Length; i++)
            {
                if (CheckMark(line[i]))
                {
                    if ((i + 1) <= (line.Length - 1) && line[i + 1] == ' ')
                    {
                        line.Remove(i, 1);
                        i--;
                    }
                    else
                    {
                        line[i] = ' ';
                    }
                }
            }
        }

        /// <summary>
        /// Проверка на пунктационнй знак
        /// </summary>
        /// <param name="symbol">символ для проверки</param>
        /// <returns>true - да он, false - нет не пунктационнй знак</returns>
        private bool CheckMark(char symbol)
        {
            for (int i = 0; i < PunctuationMarks.Length; i++)
            {
                if (symbol == PunctuationMarks[i])
                {
                    return true;
                }
            }
            return false;
        }
    }
}
