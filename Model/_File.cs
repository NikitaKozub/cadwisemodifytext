﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public abstract class _File
    {
        /// <summary>
        /// Проверяет есть ли файл, если нет создаёт, если есть удаляет и создаёт
        /// Какие бы файлы не появлялись это будет нужно всем.
        /// </summary>
        /// <param name="path">путь к файлу</param>
        protected void CheckFile(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            else
            {
                File.Delete(path);
                File.Create(path).Close();
            }
        }
    }
}
