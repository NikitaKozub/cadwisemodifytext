﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace TestModifyText
{
    [TestClass]
    public class IntegrationTests
    {
        FileTxt fileTxt;
        FileTxt modificationFile;
        string[] paths;
        string pathFolder;
        public IntegrationTests()
        {
            fileTxt = new FileTxt();
            modificationFile = new FileTxt();
            paths = new string[1];
            paths[0] = @"C:\Users\nikit\source\repos\CadwiseModifyText\Model\Source\UnitTest.txt";
            pathFolder = @"C:\Users\nikit\source\repos\CadwiseModifyText\Model\Result";
            //fileTxt.ModificationFile(paths, pathFolder, false, 5);
            
        }
        [TestMethod]
        public void TestReadFileCountLetters()
        {
            //Со знаками
            fileTxt.ModificationFile(paths, pathFolder, false, 5);
            modificationFile.ReadFile(pathFolder + "\\ResultFile1.txt");
            Console.WriteLine(modificationFile.NewText.ToString());
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("фыв4"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("фы3"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фыва5"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фывап6"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фывапу7"));
            //без знаками
            fileTxt.ModificationFile(paths, pathFolder, true, 5);
            modificationFile.ReadFile(@pathFolder + "\\ResultFile1.txt");
            Console.WriteLine(modificationFile.NewText.ToString());
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("фыв4"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("фы3"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фыва5"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фывап6"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("фывапу7"));
        }

        [TestMethod]
        public void TestReadFileNumbers()
        {
            //Со знаками
            fileTxt.ModificationFile(paths, pathFolder, false, 5);
            modificationFile.ReadFile(pathFolder + "\\ResultFile1.txt");
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("+"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("12344"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("12"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("13"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("-1"));
            //без знаками
            fileTxt.ModificationFile(paths, pathFolder, true, 5);
            modificationFile.ReadFile(pathFolder + "\\ResultFile1.txt");
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("+"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("12344"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("12"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("13"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("-1"));
        }

        [TestMethod]
        public void TestReadFilePunctuationMarks()
        {
            //Со знаками
            fileTxt.ModificationFile(paths, pathFolder, false, 5);
            modificationFile.ReadFile(pathFolder + "\\ResultFile1.txt");
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf(","));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("."));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("adafasw8"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("fafa"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf(",fas"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("fafa,fas"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("ааыпкым.Фуыс"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("ааыпкым"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("Фуыс"));
            //без знаками
            fileTxt.ModificationFile(paths, pathFolder, true, 5);
            modificationFile.ReadFile(pathFolder + "\\ResultFile1.txt");
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf(","));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("."));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("adafasw8"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("fafafas"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf(",fas"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("fafa,fas"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("ааыпкым.Фуыс"));
            Assert.AreNotEqual(-1, modificationFile.NewText.ToString().IndexOf("ааыпкым"));
            Assert.AreEqual(-1, modificationFile.NewText.ToString().IndexOf("Фуыс"));
        }
    }
}
