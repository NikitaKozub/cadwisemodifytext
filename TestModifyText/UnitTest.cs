﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestModifyText
{
    [TestClass]
    public class UnitTest
    {
        FileTxt fileTxt;
        public UnitTest()
        {
            fileTxt = new FileTxt();
        }

        /// <summary>
        /// Достаточно ли символов в слове
        /// </summary>
        [TestMethod]
        public void TestCountNumber()
        {
            fileTxt.CountNumber = 4;
            fileTxt.IsDeletePunctuationMark = false;
            fileTxt.word.Append("qwerq");
            Assert.AreEqual(true, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("qwqw");
            Assert.AreEqual(true, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("qwq");
            Assert.AreEqual(false, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("1qwq");
            Assert.AreEqual(true, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("1w");
            Assert.AreEqual(false, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append(" ");
            Assert.AreEqual(false, fileTxt.CheckCountNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("");
            Assert.AreEqual(false, fileTxt.CheckCountNumber());
        }

        /// <summary>
        /// Проверка на число 
        /// </summary>
        [TestMethod]
        public void TestCheckNumber()
        {
            fileTxt.CountNumber = 4;
            fileTxt.IsDeletePunctuationMark = false;
            fileTxt.word.Append("1");
            Assert.AreEqual(true, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("25");
            Assert.AreEqual(true, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("й12");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("-123");
            Assert.AreEqual(true, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("21w");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("21w12");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("21!12");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("!21!12!");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("!2112");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("2112!");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append(" ");
            Assert.AreEqual(false, fileTxt.CheckNumber());
            fileTxt.word.Clear();
            fileTxt.word.Append("");
            Assert.AreEqual(false, fileTxt.CheckNumber());
        }
    }
}
